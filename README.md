#### DevOps QA Techical Task

### Install dependencies
```
cd cloned project
pipenv install
```

### Run tests
```
pytest
```
# pytest will collect and all tests in folders test_*

### Run tests with allure reports
Allure Commandline should be installed first
```
pytest --alluredir=results
allure serve results
```