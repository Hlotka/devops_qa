import os

import allure
import boto3
import requests


class Boto3Helpers:

    def __init__(self):
        pass

    @allure.step('Get instance attributes')
    def get_ec2_instance_attributes(self):
        ec2_client = boto3.client('ec2')
        response = ec2_client.describe_instances()
        for reservations in response["Reservations"]:
            for instance in reservations["Instances"]:
                return instance

    @allure.step('Get elb instance attributes')
    def get_elb_instance_attributes(self, get_listener=None):
        client = boto3.client('elb')
        response = client.describe_load_balancers()
        for lbdescriptions in response["LoadBalancerDescriptions"]:
            if get_listener:
                for listeners in lbdescriptions['ListenerDescriptions']:
                    return listeners.get('Listener')
            return lbdescriptions


class Helpers:

    def __init__(self):
        pass

    @allure.step('Parse nginx.conf file')
    def parse_config(self, config, param):
        start = config.find(param) + len(param)
        end = config.find(";", start)
        param_value = config[start:end]
        return param_value

    @staticmethod
    @allure.step('Get incoming real X-Remote-Ip header')
    def return_incoming_ip():
        r = requests.head('http://' + os.environ['PUBLIC_DNS_NAME'])
        return r.headers['X-Remote-Ip']

    """ This should iterate in reverse order, 
    or also check access.log timestamp+delta to be sure current logging is working"""
    @allure.step('Check ip in access logs')
    def iterate_stdout(self, stdout, ip):
        for line in iter(stdout.readline, ""):
            if ip in line:
                return True
        return False


