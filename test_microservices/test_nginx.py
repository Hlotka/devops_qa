import os

import pytest
import requests
from utils.helpers import Helpers, Boto3Helpers


class TestNGINX:

    def test_nginx_accessible_via_external_dns(self):
        assert False

    def test_nginx_accessible_via_ssh(self):
        assert False

    @pytest.mark.parametrize('port', ['80'])
    def test_nginx_listens_80_port_via_elb(self, port):
        response = Boto3Helpers().get_elb_instance_attributes(get_listener=True)
        assert 80 == response.get('InstancePort'), 'Instance isn`t listening on port 80'

    def test_nginx_listens_80_port_via_url_port(self):
        url = 'http://' + os.environ['PUBLIC_DNS_NAME'] + ':80'
        resp = requests.head(url)
        assert 'nginx' in resp.headers['Server'], 'No nginx in response headers on request to the aws public dns name'

    @pytest.mark.parametrize('param', ['worker_processes '])
    def test_nginx_config(self, ssh_client_to_ec2_instance, param):
        stdin, stdout, stderr = ssh_client_to_ec2_instance.exec_command('cat /etc/nginx/nginx.conf')
        config = stdout.read().decode('UTF-8')
        param_value = Helpers().parse_config(config=config, param=param)
        assert 'auto' == param_value, 'Worker_processes param in nginx.conf isn`t set to auto'

    def test_nginx_writes_incoming_ip_in_access_log(self, ssh_client_to_ec2_instance):
        stdin, stdout, stderr = ssh_client_to_ec2_instance.exec_command("tail /var/log/nginx/access.log | awk '{print $1}'")
        incoming_client_ip = Helpers.return_incoming_ip()
        assert Helpers().iterate_stdout(stdout, incoming_client_ip)




