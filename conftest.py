import paramiko
import pytest


@pytest.fixture
def ssh_client_to_ec2_instance():
    ssh_client = paramiko.SSHClient()
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_client.connect('54.200.136.30', username='ubuntu')
    yield ssh_client
    ssh_client.close()




